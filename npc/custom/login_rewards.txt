-	script	LOGIN_REWARDS	-1,{
	function getTimeLeft;

	OnPCLoginEvent:
		if( ( #LOGIN_REWARD_CD + 86400 ) < gettimetick( 2 ) ) #SUCCESSIVE_LOGIN_DAYS = 0;

		if( #LOGIN_REWARD_CD < gettimetick( 2 ) ) {
			#SUCCESSIVE_LOGIN_DAYS++;
		
			if( #SUCCESSIVE_LOGIN_DAYS == 7 ) {
				.@itemId = Weekly_Login_Reward_Box_AR;
				#SUCCESSIVE_LOGIN_DAYS = 1;
			} else  {
				.@itemId = Daily_Login_Reward_Box_AR;
			}

			sleep2( 500 );
			getitem .@itemId, 1;
			#LOGIN_REWARD_CD = gettimetick( 2 ) + 86400;
			end;
		}

		dispbottom "You can get your next login reward in " + getTimeLeft( #LOGIN_REWARD_CD ) + ".";
	end;

	function getTimeLeft {
		.@secondsLeft = getarg( 0 ) - gettimetick( 2 );
		.@hours$ =   .@secondsLeft / 3600;
		.@minutes$ = ( .@secondsLeft - ( atoi( .@hours$ ) * 3600) ) / 60;
		.@seconds$ = .@secondsLeft - ( ( atoi( .@hours$ ) * 3600 ) + ( atoi( .@minutes$ ) * 60 ) );
	
		if( getstrlen( .@hours$ ) == 1 ) .@hours$ = "0" + .@hours$;
		if( getstrlen( .@minutes$ ) == 1 ) .@minutes$ = "0" + .@minutes$;
		if( getstrlen( .@seconds$ ) == 1 ) .@seconds$ = "0" + .@seconds$;
	
		return .@hours$ + ":" + .@minutes$ + ":" + .@seconds$;
	}
}