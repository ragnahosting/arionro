// Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// See the LICENSE file

// This file was automatically generated. Any edit to it will be lost.

// Platform (uname -s)
#define SYSINFO_PLATFORM "Linux"

// Operating System version (Platform-dependent)
#define SYSINFO_OSVERSION "CentOS release 6.7 (Final)"

// CPU Model (Platform-dependent)
#define SYSINFO_CPU "Intel(R) Xeon(R) CPU E3-1271 v3 @ 3.60GHz"

// CPU Cores (Platform-dependent)
#define SYSINFO_CPUCORES ( 6 )

// OS Architecture (uname -m)
#define SYSINFO_ARCH "x86_64"

// Compiler Flags
#define SYSINFO_CFLAGS "-g -O2 -pipe -ffast-math -Wall -Wextra -Wno-sign-compare -Wno-unused-parameter -Wno-clobbered -Wempty-body -Wformat-security -Wno-format-nonliteral -Wno-switch -Wno-missing-field-initializers -fPIC -fno-strict-aliasing -DMAXCONN=16384 -I../common -DHAS_TLS -DHAVE_SETRLIMIT -DHAVE_STRNLEN -I/usr/include -DHAVE_MONOTONIC_CLOCK"

// VCS Type
#define SYSINFO_VCSTYPE VCSTYPE_NONE

// VCS Revision
#define SYSINFO_VCSREV ""

